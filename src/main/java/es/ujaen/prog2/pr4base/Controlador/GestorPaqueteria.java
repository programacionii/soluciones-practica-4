/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Controlador;

import es.ujaen.prog2.pr4base.Datos.Enumerados.FranjaProgramacion;
import es.ujaen.prog2.pr4base.Datos.Enumerados.TipoEnvioExpress;
import es.ujaen.prog2.pr4base.Datos.Paquete;
import es.ujaen.prog2.pr4base.Datos.PaqueteCertificado;
import es.ujaen.prog2.pr4base.Datos.PaqueteExpress;
import es.ujaen.prog2.pr4base.Datos.PaqueteProgramado;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author UJA
 */
public class GestorPaqueteria {

    private ArrayList<Paquete> paquetes;

    public GestorPaqueteria(String archivoGuardado) throws FileNotFoundException, IOException, ClassNotFoundException {

        FileInputStream streamIn = new FileInputStream(archivoGuardado);
        ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
        paquetes = (ArrayList<Paquete>) objectinputstream.readObject();

        objectinputstream.close();
        streamIn.close();

        System.out.println("Leidos " + paquetes.size());

        for (int i = 0; i < paquetes.size(); i = i + 10000) {
            System.out.println(paquetes.get(i).getIdentificador() + " " + paquetes.get(i).getClass());
        }

    }

    public static void guardarArrayPaquetes(String archivoGuardado, ArrayList<Paquete> arrayAGuardar) throws FileNotFoundException, IOException {

        FileOutputStream fout = new FileOutputStream(archivoGuardado);
        ObjectOutputStream oos = new ObjectOutputStream(fout);
        oos.writeObject(arrayAGuardar);

        oos.close();
        fout.close();

    }

    public int eliminarRepetidos() throws IOException {
        int repetidos = 0;
        ArrayList<Paquete> temporal = new ArrayList<>(paquetes);
        ArrayList<Paquete> paqRepetidos = new ArrayList<>();
        Collections.sort(temporal);
        for (int i = 0; i < temporal.size() - 1; i++) {
            if (temporal.get(i).getIdentificador() == temporal.get(i + 1).getIdentificador()) {
                paqRepetidos.add(temporal.get(i));
                repetidos++;
            }
        }

        for (int i = 0; i < paqRepetidos.size() - 1; i++) {
            paquetes.remove(paqRepetidos.get(i));
        }

        guardarArrayPaquetes("Repetidos.txt", paqRepetidos);

        System.out.println("Quedan " + paquetes.size());

        return repetidos;
    }

    public double ordenarArray1() throws IOException {
        long inicio = System.currentTimeMillis();

        for (int i = 0; i < paquetes.size(); i++) {
            int menor = Integer.MAX_VALUE;
            int indiceMenor = i;
            for (int j = i; j < paquetes.size(); j++) {
                if (paquetes.get(j).getIdentificador() < menor) {
                    menor = paquetes.get(j).getIdentificador();
                    indiceMenor = j;
                }
            }

            Collections.swap(paquetes, i, indiceMenor);

        }

        double tiempo = 1.0 * (System.currentTimeMillis() - inicio) / 1000;

        return tiempo;
    }

    public double ordenarArray2() throws IOException {
        long inicio = System.currentTimeMillis();

        boolean continuar = true;
        int contador = 0;
        while (continuar) {
            continuar = false;
            for (int i = 0; i < paquetes.size() - 1 - contador; i++) {
                if (paquetes.get(i).getIdentificador() > paquetes.get(i + 1).getIdentificador()) {

                    Collections.swap(paquetes, i, i + 1);
                    continuar = true;
                }
            }
            contador++;
        }

        double tiempo = 1.0 * (System.currentTimeMillis() - inicio) / 1000;

        return tiempo;
    }

    public double ordenarArray3() throws IOException {
        long inicio = System.currentTimeMillis();

        Collections.sort(paquetes);
        double tiempo = 1.0 * (System.currentTimeMillis() - inicio) / 1000;

        return tiempo;
    }

    private void merge(ArrayList<Paquete> paqs, int indiceMenor, int indiceMedio, int indiceMayor) {

        ArrayList<Paquete> auxiliar = new ArrayList<>();
        int indice1 = indiceMenor;
        int indice2 = indiceMedio;

        while (indice1 < indiceMedio && indice2 < indiceMayor) {
            if (paqs.get(indice1).getIdentificador() < paqs.get(indice2).getIdentificador()) {
                auxiliar.add(paqs.get(indice1));
                indice1++;
            } else {
                auxiliar.add(paqs.get(indice2));
                indice2++;
            }
        }
        //Quedan del primer array
        if (indice1 < indiceMedio) {
            for (int i = indice1; i < indiceMedio; i++) {
                auxiliar.add(paqs.get(i));
            }
        } else {
            for (int i = indice2; i < indiceMayor; i++) {
                auxiliar.add(paqs.get(i));
            }
        }

        for (int i = 0; i < auxiliar.size(); i++) {
            paqs.set(i + indiceMenor, auxiliar.get(i));
        }

    }

    private void mergeSort(ArrayList<Paquete> paqs, int indiceMenor, int indiceMayor) {
        //el indicemayor no es inclusivo
        if (indiceMenor < indiceMayor - 1) {

            int indiceMedio = (indiceMayor + indiceMenor) / 2;
            mergeSort(paqs, indiceMenor, indiceMedio);
            mergeSort(paqs, indiceMedio, indiceMayor);//No sumo uno al indice medio por que el anterior es no inclusivo

            merge(paqs, indiceMenor, indiceMedio, indiceMayor);

        }

    }

    public double ordenarArrayMergeSort() throws IOException {
        long inicio = System.currentTimeMillis();

        mergeSort(paquetes, 0, paquetes.size());

        double tiempo = 1.0 * (System.currentTimeMillis() - inicio) / 1000;

        return tiempo;
    }

    public void imprimirInfoReducida() {

        for (int i = 0; i < 20 && i < paquetes.size(); i++) {
            System.out.println(i + "\t" + paquetes.get(i));
        }

        for (int i = paquetes.size() - 20 < 0 ? 0 : paquetes.size() - 20; i < paquetes.size(); i++) {
            System.out.println(i + "\t" + paquetes.get(i));
        }

    }

    public Paquete búsquedaIterativa(int idBuscado) {

        for (Paquete paq : paquetes) {
            if (paq.getIdentificador() == idBuscado) {
                return paq;
            }
        }

        return null;
    }

    private Paquete busquedaBinaria(int idBuscado, ArrayList<Paquete> paqs, int indiceMenor, int indiceMayor) {
        //el indicemayor no es inclusivo
        if (indiceMenor < indiceMayor - 1) {//Queda mas de un elemento

            int indiceMedio = (indiceMayor + indiceMenor) / 2;
            if (paqs.get(indiceMedio).getIdentificador() == idBuscado) { //Si el medio es el que estamos buscando
                return paqs.get(indiceMedio);
            }

            if (paqs.get(indiceMedio).getIdentificador() > idBuscado) {
                return busquedaBinaria(idBuscado, paqs, indiceMenor, indiceMedio);
            } else {
                return busquedaBinaria(idBuscado, paqs, indiceMedio + 1, indiceMayor);//+1 porque sabemos que el medio no es
            }

        } else if (indiceMenor == indiceMayor - 1) {//Queda un elemento

            if (paqs.get(indiceMenor).getIdentificador() == idBuscado) {//Es el que buscamos
                return paqs.get(indiceMenor);

            } else {//No es el que buscamos
                return null;
            }
        }

        return null;
    }

    public Paquete búsquedaBinariaRecursiva(int idBuscado) {

        return busquedaBinaria(idBuscado, paquetes, 0, paquetes.size());
    }

    @Override
    public String toString() {
        return paquetes.toString();
    }

    public static void generarAleatorios() throws IOException {
        ArrayList<String> nombresArray = new ArrayList<>(Arrays.asList("Antonio", "José", "Manuel", "Francisco", "David", "Juan", "JoséAntonio", "Javier", "JoséLuis", "Daniel", "FranciscoJavier", "Jesús", "Carlos", "Alejandro", "Miguel", "JoseManuel", "Rafael", "Pedro", "MiguelÁngel", "Ángel", "JoséMaría", "Pablo", "Fernando", "Sergio", "Luis", "Jorge", "Alberto", "JuanCarlos", "JuanJosé", "Álvaro", "Diego", "Adrián", "Raúl", "JuanAntonio", "Enrique", "Iván", "Ramón", "Rubén", "Vicente", "Óscar", "Andrés", "Joaquín", "JuanManuel", "Santiago", "Eduardo", "Víctor", "Roberto", "Mario", "Jaime", "FranciscoJosé", "Ignacio", "Marcos", "Alfonso", "Salvador", "Ricardo", "Jordi", "Emilio", "Guillermo", "Julián", "Gabriel", "Julio", "Hugo", "Tomás", "Marc", "José Miguel", "Agustín", "Gonzalo", "Mohamed", "José Ramón", "Félix", "Joan", "Nicolás", "Ismael", "Cristian", "Martín", "Samuel", "Aitor", "Josep", "Juan Francisco", "Mariano", "Domingo", "Héctor", "José Carlos", "Alfredo", "Sebastián", "César", "Iker", "Felipe", "José Ángel", "José Ignacio", "Alex", "Victor Manuel", "Lucas", "Luis Miguel", "Rodrigo", "Gregorio", "José Francisco", "Juan Luis", "Xavier", "Albert", "María Carmen", "María", "Carmen", "Josefa", "Ana María", "Isabel", "María Pilar", "María Dolores", "María Teresa", "Laura", "Ana", "Cristina", "María Ángeles", "Francisca", "Marta", "Antonia", "Dolores", "María Isabel", "María José", "Lucia", "María Luisa", "Sara", "Paula", "Elena", "Pilar", "Concepción", "Raquel", "Rosa María", "Manuela", "Mercedes", "María Jesús", "Rosario", "Juana", "Teresa", "Beatriz", "Encarnación", "Nuria", "Julia", "Silvia", "Irene", "Montserrat", "Patricia", "Alba", "Rosa", "Andrea", "Rocío", "Mónica", "María Mar", "Ángela", "Alicia", "Sonia", "Sandra", "Marina", "Susana", "Margarita", "Yolanda", "Natalia", "María Josefa", "María Rosario", "Inmaculada", "Eva", "María Mercedes", "Claudia", "Ana Isabel", "Esther", "Noelia", "Ángeles", "Verónica", "Carla", "Sofía", "Carolina", "Nerea", "María Rosa", "María Victoria", "Amparo", "Eva María", "María Concepción", "Lorena", "Miriam", "Inés", "Ana Belén", "Victoria", "María Elena", "María Antonia", "Catalina", "Consuelo", "Lidia", "María Nieves", "Daniela", "Celia", "Emilia", "Luisa", "Gloria", "Olga", "Alejandra", "Aurora", "María Soledad", "Esperanza", "Ainhoa", "María Cristina"));
        ArrayList<String> apellidosArray = new ArrayList<>(Arrays.asList("García", "González", "Rodríguez", "Fernández", "López", "Martínez", "Sánchez", "Pérez", "Gómez", "Martin", "Jiménez", "Ruiz", "Hernández", "Diaz", "Moreno", "Muñoz", "Álvarez", "Romero", "Alonso", "Gutiérrez", "Navarro", "Torres", "Domínguez", "Vázquez", "Ramos", "Gil", "Ramírez", "Serrano", "Blanco", "Molina", "Morales", "Suarez", "Ortega", "Delgado", "Castro", "Ortiz", "Rubio", "Marín", "Sanz", "Núñez", "Iglesias", "Medina", "Garrido", "Cortes", "Castillo", "Santos", "Lozano", "Guerrero", "Cano", "Prieto", "Méndez", "Cruz", "Calvo", "Gallego", "Vidal", "León", "Márquez", "Herrera", "Peña", "Flores", "Cabrera", "Campos", "Vega", "Fuentes", "Carrasco", "Diez", "Caballero", "Reyes", "Nieto", "Aguilar", "Pascual", "Santana", "Herrero", "Lorenzo", "Montero", "Hidalgo", "Giménez", "Ibáñez", "Ferrer", "Duran", "Santiago", "Benítez", "Mora", "Vicente", "Vargas", "Arias", "Carmona", "Crespo", "Román", "Pastor", "Soto", "Sáez", "Velasco", "Moya", "Soler", "Parra", "Esteban", "Bravo", "Gallardo", "Rojas"));
        ArrayList<String> calleArray = new ArrayList<>(Arrays.asList("Bidasoa", "Nervión", "Ibaizábal", "Asón", "Miera", "Pas", "Saja", "Besaya", "Nansa", "Deva", "Sella", "Nalón", "Navia", "Eo", "Tambre", "Ulla", "Eume", "Léez", "Miño", "Duero", "Tajo", "Guadiana", "Piedras", "Odiel", "Tinto", "Guadalquivir", "Guadalete", "Barbate", "Guadiaro", "Guadalhorce", "Guadalmedina", "Vélez", "Guadalfeo", "Adra", "Andarax", "Almanzora", "Segura", "Vinalopó", "Serpis", "Júcar", "Turia", "Palancia", "Mijares", "Ebro", "Francolí", "Gayá", "Llobregat", "Tordera", "Ter", "Fluviá", "Muga"));
        ArrayList<Paquete> paquetes = new ArrayList<>();

        Random rand = new Random(1234567890);

        for (int i = 0; i < 25000; i++) {
            int identificador = rand.nextInt(1000000) + 1000000;
            String direccionEntrega = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String direccionRemitente = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String destinatario = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            String remitente = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            LocalDate recogida = rand.nextInt(3) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            LocalDate entrega = rand.nextInt(4) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            Paquete p = new Paquete(identificador, direccionEntrega, direccionRemitente, destinatario, remitente, recogida, entrega);
            paquetes.add(p);
            if (i % 35 == 0) {
                paquetes.add(p);
            }
        }

        for (int i = 0; i < 25000; i++) {
            int identificador = rand.nextInt(1000000) + 1000000;
            String direccionEntrega = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String direccionRemitente = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String destinatario = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            String remitente = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            LocalDate recogida = rand.nextInt(3) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            LocalDate entrega = rand.nextInt(4) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            TipoEnvioExpress tipoEnvioExpress = TipoEnvioExpress.getTipoDesdeInt(rand.nextInt(5));
            Paquete p = new PaqueteExpress(tipoEnvioExpress, identificador, direccionEntrega, direccionRemitente, destinatario, remitente, recogida, entrega);
            paquetes.add(p);
            if (i % 35 == 0) {
                paquetes.add(p);
            }
        }

        for (int i = 0; i < 25000; i++) {
            int identificador = rand.nextInt(1000000) + 1000000;
            String direccionEntrega = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String direccionRemitente = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String destinatario = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            String remitente = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            LocalDate recogida = rand.nextInt(3) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            LocalDate entrega = rand.nextInt(4) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            Paquete p = new PaqueteCertificado(generarDNI(rand), identificador, direccionEntrega, direccionRemitente, destinatario, remitente, recogida, entrega);
            paquetes.add(p);
            if (i % 35 == 0) {
                paquetes.add(p);
            }
        }

        for (int i = 0; i < 25000; i++) {
            int identificador = rand.nextInt(1000000) + 1000000;
            String direccionEntrega = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String direccionRemitente = calleArray.get(rand.nextInt(calleArray.size())) + " " + String.valueOf(rand.nextInt(150) + 1) + ", " + String.valueOf(rand.nextInt(1000) + 27000);
            String destinatario = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            String remitente = nombresArray.get(rand.nextInt(nombresArray.size())) + " " + nombresArray.get(rand.nextInt(nombresArray.size())) + " " + apellidosArray.get(rand.nextInt(apellidosArray.size()));
            LocalDate recogida = rand.nextInt(3) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            LocalDate entrega = rand.nextInt(4) < 2 ? LocalDate.parse("2021-12-0" + (rand.nextInt(9) + 1)) : null;
            FranjaProgramacion franja = FranjaProgramacion.getFranjaDesdeInt(rand.nextInt(5));
            Paquete p = new PaqueteProgramado(franja, identificador, direccionEntrega, direccionRemitente, destinatario, remitente, recogida, entrega);
            paquetes.add(p);
            if (i % 35 == 0) {
                paquetes.add(p);
            }
        }

        Collections.shuffle(paquetes, rand);

        guardarArrayPaquetes("Paquetes.txt", paquetes);

    }

    private static String generarDNI(Random rand) {
        String dni = "";
        for (int i = 0; i < 8; i++) {
            dni += rand.nextInt(10);
        }
        return dni;
    }

}
