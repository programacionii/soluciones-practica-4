/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base;

import es.ujaen.prog2.pr4base.Controlador.GestorPaqueteria;
import es.ujaen.prog2.pr4base.Datos.Paquete;
import es.ujaen.prog2.pr4base.Datos.PaqueteProgramado;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author UJA
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {

        //GestorPaqueteria.generarAleatorios();
        GestorPaqueteria gestor = new GestorPaqueteria("Paquetes.txt");
        gestor.imprimirInfoReducida();

        //ELIMINACION DE REPETIDOS
        int repetidos = gestor.eliminarRepetidos();
        System.out.println("Repetidos " + repetidos);

        //ORDENADO
        //Dejar solo uno de los algoritmos descomentado y el resto comentado 
//        double tiempoOrden1 = gestor.ordenarArray1();
//        System.out.println("Tiempo Ordenar 1: " + tiempoOrden1);
//        double tiempoOrden2 = gestor.ordenarArray2();
//        System.out.println("Tiempo Ordenar 2: " + tiempoOrden2);
//        double tiempoOrden3 = gestor.ordenarArray3();
//        System.out.println("Tiempo Ordenar 3: " + tiempoOrden3);
//        
        double tiempoOrden4 = gestor.ordenarArrayMergeSort();
        System.out.println("Tiempo Ordenar 4: " + tiempoOrden4);

        gestor.imprimirInfoReducida();

        ArrayList<Integer> identificadoresBusqueda = new ArrayList<>(Arrays.asList(1219410, 1374735, 1440611, 1432471, 1646167, 1457931, 1084871, 1057095, 1681299, 1591899));
        //BUSQUEDA ITERATIVA
        long inicioBusqIter = System.currentTimeMillis();

        ArrayList<Paquete> encontradosIter = new ArrayList<>();
        for (int i = 0; i < identificadoresBusqueda.size(); i++) {
            Paquete encontrado = gestor.búsquedaIterativa(identificadoresBusqueda.get(i));
            if (encontrado != null) {
                encontradosIter.add(encontrado);
            }
        }

        double tiempoBusqIter = 1.0 * (System.currentTimeMillis() - inicioBusqIter) / 1000;
        System.out.println("Tiempo búsqueda iterativa: " + tiempoBusqIter);

        //Imprimir la información de los paquetes encontrados que sean de tipo programado
        for (int i = 0; i < encontradosIter.size(); i++) {
            if (encontradosIter.get(i) instanceof PaqueteProgramado) {
                System.out.println(encontradosIter.get(i));
            }
        }

        //BUSQUEDA RECURSIVA
        long inicioBusqRec = System.currentTimeMillis();

        ArrayList<Paquete> encontradosRec = new ArrayList<>();
        for (int paq : identificadoresBusqueda) {
            Paquete encontrado = gestor.búsquedaBinariaRecursiva(paq);
            if (encontrado != null) {
                encontradosRec.add(encontrado);
            }
        }

        double tiempoBusqRec = 1.0 * (System.currentTimeMillis() - inicioBusqRec) / 1000;
        System.out.println("Tiempo búsqueda recursiva: " + tiempoBusqRec);

        //Imprimir la información de los paquetes encontrados que solo sean de tipo Paquete y no subclases
        for (Paquete paq : encontradosRec) {
            if (paq.getClass() == Paquete.class) {
                System.out.println(paq);
            }
        }

    }

}
