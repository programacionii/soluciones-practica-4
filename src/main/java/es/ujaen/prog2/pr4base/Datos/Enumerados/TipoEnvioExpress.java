/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Datos.Enumerados;

/**
 *
 * @author UJA
 */
public enum TipoEnvioExpress {
    E12HORAS, E24HORAS, E48HORAS, E72HORAS;

    @Override
    public String toString() {
        switch (this) {
            case E12HORAS:
                return "12Horas";
            case E24HORAS:
                return "24Horas";
            case E48HORAS:
                return "48Horas";
            case E72HORAS:
                return "72Horas";
        }
        return "ERROR";
    }

    public static TipoEnvioExpress getTipoDesdeInt(int n) {
        switch (n) {
            case (0):
                return E12HORAS;
            case (1):
                return E24HORAS;
            case (2):
                return E48HORAS;
            case (3):
                return E72HORAS;
        }
        return E72HORAS;
    }
}
