/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Datos.Enumerados;

/**
 *
 * @author UJA
 */
public enum FranjaProgramacion {
    DE8A10, DE10A12, DE12A14, DE16A18;

    @Override
    public String toString() {
        switch (this) {
            case DE8A10:
                return "08:00 - 10:00";
            case DE10A12:
                return "10:00 - 12:00";
            case DE12A14:
                return "12:00 - 14:00";
            case DE16A18:
                return "16:00 - 18:00";
        }
        return "ERROR";
    }

    public static FranjaProgramacion getFranjaDesdeInt(int n) {
        switch (n) {
            case (0):
                return DE8A10;
            case (1):
                return DE10A12;
            case (2):
                return DE12A14;
            case (3):
                return DE16A18;
        }
        return DE16A18;
    }
}
