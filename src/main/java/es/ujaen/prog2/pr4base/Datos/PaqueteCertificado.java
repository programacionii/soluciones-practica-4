/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Datos;

import java.time.LocalDate;

/**
 *
 * @author UJA
 */
public class PaqueteCertificado extends Paquete {

    private String dniDestinatario;

    public PaqueteCertificado(String dniDestinatario, int identificador, String direccionEntrega, String direccionRemitente, String destinatario, String remitente, LocalDate fechaRecogida, LocalDate fechaEntrega) {
        super(identificador, direccionEntrega, direccionRemitente, destinatario, remitente, fechaRecogida, fechaEntrega);
        this.dniDestinatario = dniDestinatario;
    }

    @Override
    public String toString() {
        return super.toString() + " | DNI destinatario: " + dniDestinatario;
    }

}
