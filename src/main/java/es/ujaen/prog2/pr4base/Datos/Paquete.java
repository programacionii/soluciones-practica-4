/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Datos;

import java.io.Serializable;
import java.time.LocalDate;

/**
 *
 * @author UJA
 */
public class Paquete implements Serializable, Comparable<Paquete>{

    private int identificador;
    private String direccionEntrega;
    private String direccionRemitente;
    private String destinatario;
    private String remitente;
    private LocalDate fechaRecogida;
    private LocalDate fechaEntrega;

    public Paquete(int identificador, String direccionEntrega, String direccionRemitente, String destinatario, String remitente, LocalDate fechaRecogida, LocalDate fechaEntrega) {
        this.identificador = identificador;
        this.direccionEntrega = direccionEntrega;
        this.direccionRemitente = direccionRemitente;
        this.destinatario = destinatario;
        this.remitente = remitente;
        this.fechaRecogida = fechaRecogida;
        this.fechaEntrega = fechaEntrega;
    }

    @Override
    public String toString() {
        String resultado = "id: " + identificador
                + " | destinatario: " + destinatario + ", " + direccionEntrega
                + " | remitente: " + remitente + ", " + direccionRemitente
                + " | recogido el: " + (fechaRecogida != null ? fechaRecogida : "NO RECOGIDO")
                + " | entregado el: " + (fechaEntrega != null ? fechaEntrega : "NO ENTREGADO");
        return resultado; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Paquete o) {
        return identificador - o.identificador;
    }

    public int getIdentificador() {
        return identificador;
    }  
    
}
