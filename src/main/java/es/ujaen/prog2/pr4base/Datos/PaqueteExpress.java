/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.prog2.pr4base.Datos;

import es.ujaen.prog2.pr4base.Datos.Enumerados.TipoEnvioExpress;
import java.time.LocalDate;

/**
 *
 * @author UJA
 */
public class PaqueteExpress extends Paquete {

    private TipoEnvioExpress tipoEnvio;

    public PaqueteExpress(TipoEnvioExpress tipoEnvio, int identificador, String direccionEntrega, String direccionRemitente, String destinatario, String remitente, LocalDate fechaRecogida, LocalDate fechaEntrega) {
        super(identificador, direccionEntrega, direccionRemitente, destinatario, remitente, fechaRecogida, fechaEntrega);
        this.tipoEnvio = tipoEnvio;
    }

    @Override
    public String toString() {
        return super.toString() + " | tiempo envio: " + tipoEnvio;
    }

}
